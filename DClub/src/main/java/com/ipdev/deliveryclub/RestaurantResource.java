package com.ipdev.deliveryclub;

import static java.util.Collections.singletonList;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.naming.NamingException;
import javax.persistence.EntityManager;
import javax.ws.rs.Path;

import org.apache.log4j.Logger;

import com.ipdev.domain.Restaurant;

@Path("/restaurant")
public class RestaurantResource extends ResourceBase<Restaurant> {

	private final static Logger logger = Logger.getLogger(RestaurantResource.class);

	private EntityManager em;
	private List<Restaurant> restaurants;

	@Override
	protected List getAllQuery() throws SQLException, NamingException {
		em = getEntityManager();
		em.getTransaction().begin();
		restaurants = em.createNamedQuery("Restaurant.findAll", Restaurant.class).getResultList();
		em.getTransaction().commit();
		em.close();
		return restaurants;
	}

	@Override
	protected List getSingleQuery(int id) throws NamingException {
		em = getEntityManager();
		em.getTransaction().begin();
		restaurants = singletonList(em.find(Restaurant.class, id));
		em.getTransaction().commit();
		em.close();
		return restaurants;
	}

	@Override
	protected void createQuery(Restaurant t) throws SQLException, NamingException {
		em = getEntityManager();
		em.getTransaction().begin();

		Restaurant restaurant = new Restaurant();
		restaurant.setName(t.getName());
		restaurant.setStatus(t.getStatus());
		restaurant.setDateCreated(new Date());
		restaurant.setDateOpened(t.getDateOpened());
		restaurant.setDateClosed(t.getDateClosed());
		restaurant.setImg(t.getImg());
		restaurant.setLogo(t.getLogo());
		em.persist(restaurant);

		em.getTransaction().commit();
		em.close();
	}

	@Override
	protected void deleteQuery(int id) throws SQLException, NamingException {
		em = getEntityManager();
		em.getTransaction().begin();

		Restaurant restaurant = new Restaurant();
		restaurant = em.find(Restaurant.class, id);
		em.remove(restaurant);

		em.getTransaction().commit();
		em.close();
	}

	@Override
	protected void updateQuery(Restaurant t, int id) throws SQLException, NamingException {
		em = getEntityManager();
		em.getTransaction().begin();

		Restaurant restaurant = new Restaurant();
		restaurant = em.find(Restaurant.class, id);
		restaurant.setName(t.getName() != null ? t.getName() : restaurant.getName());
		restaurant.setDateOpened(t.getDateOpened() != null ? t.getDateOpened() : restaurant.getDateOpened());
		restaurant.setDateClosed(t.getDateClosed() != null ? t.getDateClosed() : restaurant.getDateClosed());
		restaurant.setImg(t.getImg() != null ? t.getImg() : restaurant.getImg());
		restaurant.setLogo(t.getLogo() != null ? t.getLogo() : restaurant.getLogo());
		restaurant.setStatus(t.getStatus() != null ? t.getStatus() : restaurant.getStatus());
		em.persist(restaurant);

		em.getTransaction().commit();
		em.close();
	}

	@Override
	protected List getDataByNamedQuery(String sql, Map<String, Object> parms) throws SQLException, NamingException {
		// TODO Auto-generated method stub
		return null;
	}

}
