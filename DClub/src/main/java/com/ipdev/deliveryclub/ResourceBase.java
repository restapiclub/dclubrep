package com.ipdev.deliveryclub;

import java.lang.reflect.ParameterizedType;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.naming.NamingException;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.log4j.Logger;

import com.ipdev.db.DataBaseProvider;
import com.ipdev.reflection.ParameterizedTypeUtil;
import com.ipdev.security.Secured;

@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
public abstract class ResourceBase<T> {

	private final static Logger logger = Logger.getLogger(ResourceBase.class);

	protected abstract List getAllQuery() throws SQLException, NamingException;

	protected abstract List getSingleQuery(int id) throws NamingException;

	protected abstract List getDataByNamedQuery(String sql, Map<String, Object> parms)
			throws SQLException, NamingException;

	protected abstract void createQuery(T t) throws SQLException, NamingException;

	protected abstract void deleteQuery(int id) throws SQLException, NamingException;

	protected abstract void updateQuery(T t, int id) throws SQLException, NamingException;

	protected EntityManager getEntityManager() throws NamingException {
		return DataBaseProvider.getEmf().createEntityManager();
	}

	@GET
	public Response getList() throws SQLException, NamingException {
		logger.info("Start Call findAll query...");
		List<T> records = getAllQuery();
		if (records == null)
			return Response.status(Status.NOT_FOUND)
					.entity(new GenericEntity<List<T>>(new ArrayList<T>(), ParameterizedTypeUtil.getType(
							((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0])))
					.build();

		GenericEntity<List<T>> entity = new GenericEntity<List<T>>(records, ParameterizedTypeUtil
				.getType(((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0])) {
		};
		logger.info("End Call findAll query...");
		return Response.ok(entity).build();
	}

	@GET
	@Path("{id}")
	public Response getSingle(@PathParam("id") int id) throws NamingException {
		logger.info("Start Call findSingle query...");
		List<T> records = getSingleQuery(id);
		if (records.get(0) == null)
			return Response.status(Status.NOT_FOUND)
					.entity(new GenericEntity<List<T>>(new ArrayList<T>(), ParameterizedTypeUtil.getType(
							((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0])))
					.build();

		GenericEntity<List<T>> entity = new GenericEntity<List<T>>(records, ParameterizedTypeUtil
				.getType(((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0])) {
		};
		logger.info("End Call findSingle query...");
		return Response.ok(entity).build();
	}

	@Secured
	@POST
	public Response insert(T t) {
		try {
			logger.info("Start Call insertSingle query...");
			createQuery(t);
			logger.info("End Call insertSingle query...");
			return Response.status(Status.CREATED).entity(t).build();
		} catch (SQLException | NamingException e) {
			logger.error(e.getMessage());
			return Response.status(Status.NOT_MODIFIED).build();
		} catch (PersistenceException pe) {
			logger.error(pe.getMessage());
			return Response.status(Status.NOT_ACCEPTABLE).build();
		} catch (Exception ge) {
			logger.error(ge.getMessage());
			return Response.status(Status.FORBIDDEN).build();
		}
	}

	@Secured
	@DELETE
	@Path("{id}")
	public Response delete(@PathParam("id") int id) {
		try {
			logger.info("Start Call removeSingle query...");
			deleteQuery(id);
			if (getSingleQuery(id).get(0) == null)
				return Response.ok().build();
			logger.info("End Call removeSingle query...");
			return Response.status(Status.EXPECTATION_FAILED).build();
		} catch (SQLException | NamingException e) {
			logger.error(e.getMessage());
			return Response.status(Status.NOT_MODIFIED).build();
		} catch (IllegalArgumentException iae) {
			logger.error(iae.getMessage());
			return Response.status(Status.NOT_ACCEPTABLE).build();
		} catch (Exception ge) {
			logger.error(ge.getMessage());
			return Response.status(Status.FORBIDDEN).build();
		}
	}

	@Secured
	@PUT
	@Path("{id}")
	public Response update(T t, @PathParam("id") int id) {
		try {
			logger.info("Start Call updateSingle query...");
			updateQuery(t, id);
			logger.info("End Call updateSingle query...");
			return Response.ok(t).build();
		} catch (SQLException | NamingException e) {
			logger.error(e.getMessage());
			return Response.status(Status.NOT_MODIFIED).build();
		} catch (PersistenceException pe) {
			logger.error(pe.getMessage());
			return Response.status(Status.NOT_ACCEPTABLE).build();
		} catch (Exception ge) {
			logger.error(ge.getMessage());
			return Response.status(Status.FORBIDDEN).build();
		}
	}

	// @POST
	// @Path("namedQuery")
	// public List getDataByQuery(String namedQuery, Map<String, Object> parms)
	// throws SQLException, NamingException {
	// logger.info("Start Call namedQuery query...");
	// List list = getDataByNamedQuery(namedQuery, parms);
	// logger.info("End Call namedQuery query...");
	// return list;
	// }

}