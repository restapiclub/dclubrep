package com.ipdev.deliveryclub;

import static java.util.Collections.singletonList;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import javax.naming.NamingException;
import javax.persistence.EntityManager;
import javax.ws.rs.Path;

import org.apache.log4j.Logger;

import com.ipdev.domain.Item;

@Path("/item")
public class ItemResource extends ResourceBase<Item> {

	private final static Logger logger = Logger.getLogger(ItemResource.class);

	private EntityManager em;
	private List<Item> items;

	@Override
	protected List getAllQuery() throws SQLException, NamingException {
		em = getEntityManager();
		em.getTransaction().begin();
		items = em.createNamedQuery("Item.findAll", Item.class).getResultList();
		em.getTransaction().commit();
		em.close();
		return items;
	}

	@Override
	protected List getSingleQuery(int id) throws NamingException {
		em = getEntityManager();
		em.getTransaction().begin();
		items = singletonList(em.find(Item.class, id));
		em.getTransaction().commit();
		em.close();
		return items;
	}

	@Override
	protected void createQuery(Item t) throws SQLException, NamingException {
		em = getEntityManager();
		em.getTransaction().begin();

		Item item = new Item();
		item.setName(t.getName());
		item.setPrice(t.getPrice());
		item.setMeasurement(t.getMeasurement());
		item.setDescription(t.getDescription());
		item.setQuantity(t.getQuantity());
		item.setImg(t.getImg());
		item.setMenu(t.getMenu());
		item.setDiscount(t.getDiscount());
		em.persist(item);

		em.getTransaction().commit();
		em.close();
	}

	@Override
	protected void deleteQuery(int id) throws SQLException, NamingException {
		em = getEntityManager();
		em.getTransaction().begin();

		Item item = new Item();
		item = em.find(Item.class, id);
		em.remove(item);

		em.getTransaction().commit();
		em.close();
	}

	@Override
	protected void updateQuery(Item t, int id) throws SQLException, NamingException {
		em = getEntityManager();
		em.getTransaction().begin();

		Item item = new Item();
		item = em.find(Item.class, id);
		item.setName(t.getName() != null ? t.getName() : item.getName());
		item.setPrice(t.getPrice() != null ? t.getPrice() : item.getPrice());
		item.setMeasurement(t.getMeasurement() != null ? t.getMeasurement() : item.getMeasurement());
		item.setDescription(t.getDescription() != null ? t.getDescription() : item.getDescription());
		item.setQuantity(t.getQuantity() != null ? t.getQuantity() : item.getQuantity());
		item.setImg(t.getImg() != null ? t.getImg() : item.getImg());
		item.setMenu(t.getMenu() != null ? t.getMenu() : item.getMenu());
		item.setDiscount(t.getDiscount() != null ? t.getDiscount() : item.getDiscount());
		em.persist(item);

		em.getTransaction().commit();
		em.close();
	}

	@Override
	protected List getDataByNamedQuery(String sql, Map<String, Object> parms) throws SQLException, NamingException {
		// TODO Auto-generated method stub
		return null;
	}

}
