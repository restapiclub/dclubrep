package com.ipdev.deliveryclub;

import static java.util.Collections.singletonList;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.naming.NamingException;
import javax.persistence.EntityManager;
import javax.ws.rs.Path;

import org.apache.log4j.Logger;

import com.ipdev.domain.User;

@Path("/user")
public class UserResource extends ResourceBase<User> {

	private final static Logger logger = Logger.getLogger(UserResource.class);

	private EntityManager em;
	private List<User> users;

	@Override
	protected List getAllQuery() throws SQLException, NamingException {
		em = getEntityManager();
		em.getTransaction().begin();
		users = em.createNamedQuery("User.findAll", User.class).getResultList();
		em.getTransaction().commit();
		em.close();
		return users;
	}

	@Override
	protected List getSingleQuery(int id) throws NamingException {
		em = getEntityManager();
		em.getTransaction().begin();
		users = singletonList(em.find(User.class, id));
		em.getTransaction().commit();
		em.close();
		return users;
	}

	@Override
	protected void createQuery(User t) throws SQLException, NamingException {
		em = getEntityManager();
		em.getTransaction().begin();

		User user = new User();
		user.setDateCreated(new Date());
		user.setDateDeleted(t.getDateDeleted());
		user.setLogin(t.getLogin());
		user.setPassword(t.getPassword());
		user.setName(t.getName());
		user.setStatus(t.getStatus());
		em.persist(user);

		em.getTransaction().commit();
		em.close();
	}

	@Override
	protected void deleteQuery(int id) throws SQLException, NamingException {
		em = getEntityManager();
		em.getTransaction().begin();

		User user = new User();
		user = em.find(User.class, id);
		em.remove(user);

		em.getTransaction().commit();
		em.close();
	}

	@Override
	protected void updateQuery(User t, int id) throws SQLException, NamingException {
		em = getEntityManager();
		em.getTransaction().begin();

		User user = new User();
		user = em.find(User.class, id);
		user.setDateDeleted(t.getDateDeleted() != null ? t.getDateDeleted() : user.getDateDeleted());
		user.setPassword(t.getPassword() != null ? t.getPassword() : user.getPassword());
		user.setName(t.getName() != null ? t.getName() : user.getName());
		user.setStatus(t.getStatus() != null ? t.getStatus() : user.getStatus());
		em.persist(user);

		em.getTransaction().commit();
		em.close();
	}

	@Override
	protected List getDataByNamedQuery(String sql, Map<String, Object> parms) throws SQLException, NamingException {
		// TODO Auto-generated method stub
		return null;
	}

}
