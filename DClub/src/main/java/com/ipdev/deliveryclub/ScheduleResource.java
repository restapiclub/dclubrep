package com.ipdev.deliveryclub;

import static java.util.Collections.singletonList;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import javax.naming.NamingException;
import javax.persistence.EntityManager;
import javax.ws.rs.Path;

import org.apache.log4j.Logger;

import com.ipdev.domain.Schedule;

@Path("/schedule")
public class ScheduleResource extends ResourceBase<Schedule> {

	private final static Logger logger = Logger.getLogger(ScheduleResource.class);

	private EntityManager em;
	private List<Schedule> schedules;

	@Override
	protected List getAllQuery() throws SQLException, NamingException {
		em = getEntityManager();
		em.getTransaction().begin();
		schedules = em.createNamedQuery("Schedule.findAll", Schedule.class).getResultList();
		em.getTransaction().commit();
		em.close();
		return schedules;
	}

	@Override
	protected List getSingleQuery(int id) throws NamingException {
		em = getEntityManager();
		em.getTransaction().begin();
		schedules = singletonList(em.find(Schedule.class, id));
		em.getTransaction().commit();
		em.close();
		return schedules;
	}

	@Override
	protected void createQuery(Schedule t) throws SQLException, NamingException {
		em = getEntityManager();
		em.getTransaction().begin();

		Schedule schedule = new Schedule();
		schedule.setDayStart(t.getDayStart());
		schedule.setDayEnd(t.getDayEnd());
		schedule.setDayWeek(t.getDayWeek());
		schedule.setRestaurant(t.getRestaurant());
		em.persist(schedule);

		em.getTransaction().commit();
		em.close();
	}

	@Override
	protected void deleteQuery(int id) throws SQLException, NamingException {
		em = getEntityManager();
		em.getTransaction().begin();

		Schedule schedule = new Schedule();
		schedule = em.find(Schedule.class, id);
		em.remove(schedule);

		em.getTransaction().commit();
		em.close();
	}

	@Override
	protected void updateQuery(Schedule t, int id) throws SQLException, NamingException {
		em = getEntityManager();
		em.getTransaction().begin();

		Schedule schedule = new Schedule();
		schedule = em.find(Schedule.class, id);
		schedule.setDayStart(t.getDayStart() != null ? t.getDayStart() : schedule.getDayStart());
		schedule.setDayEnd(t.getDayEnd() != null ? t.getDayEnd() : schedule.getDayEnd());
		em.persist(schedule);

		em.getTransaction().commit();
		em.close();
	}

	@Override
	protected List getDataByNamedQuery(String sql, Map<String, Object> parms) throws SQLException, NamingException {
		// TODO Auto-generated method stub
		return null;
	}

}
