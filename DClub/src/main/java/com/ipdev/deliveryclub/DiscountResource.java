package com.ipdev.deliveryclub;

import static java.util.Collections.singletonList;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import javax.naming.NamingException;
import javax.persistence.EntityManager;
import javax.ws.rs.Path;

import org.apache.log4j.Logger;

import com.ipdev.domain.Discount;

@Path("/discount")
public class DiscountResource extends ResourceBase<Discount> {

	private final static Logger logger = Logger.getLogger(DiscountResource.class);

	private EntityManager em;
	private List<Discount> discounts;

	@Override
	protected List getAllQuery() throws SQLException, NamingException {
		em = getEntityManager();
		em.getTransaction().begin();
		discounts = em.createNamedQuery("Discount.findAll", Discount.class).getResultList();
		em.getTransaction().commit();
		em.close();
		return discounts;
	}

	@Override
	protected List getSingleQuery(int id) throws NamingException {
		em = getEntityManager();
		em.getTransaction().begin();
		discounts = singletonList(em.find(Discount.class, id));
		em.getTransaction().commit();
		em.close();
		return discounts;
	}

	@Override
	protected void createQuery(Discount t) throws SQLException, NamingException {
		em = getEntityManager();
		em.getTransaction().begin();

		Discount discount = new Discount();
		discount.setName(t.getName());
		discount.setAmount(t.getAmount());
		discount.setDateStart(t.getDateStart());
		discount.setDateEnd(t.getDateEnd());
		discount.setDescription(t.getDescription());
		discount.setType(t.getType());
		discount.setRestaurant(t.getRestaurant());
		em.persist(discount);

		em.getTransaction().commit();
		em.close();
	}

	@Override
	protected void deleteQuery(int id) throws SQLException, NamingException {
		em = getEntityManager();
		em.getTransaction().begin();

		Discount discount = new Discount();
		discount = em.find(Discount.class, id);
		em.remove(discount);

		em.getTransaction().commit();
		em.close();
	}

	@Override
	protected void updateQuery(Discount t, int id) throws SQLException, NamingException {
		em = getEntityManager();
		em.getTransaction().begin();

		Discount discount = new Discount();
		discount = em.find(Discount.class, id);
		discount.setName(t.getName() != null ? t.getName() : discount.getName());
		discount.setAmount(t.getAmount() != null ? t.getAmount() : discount.getAmount());
		discount.setDateStart(t.getDateStart() != null ? t.getDateStart() : discount.getDateStart());
		discount.setDateEnd(t.getDateEnd() != null ? t.getDateEnd() : discount.getDateEnd());
		discount.setDescription(t.getDescription() != null ? t.getDescription() : discount.getDescription());
		discount.setType(t.getType() != null ? t.getType() : discount.getType());
		em.persist(discount);

		em.getTransaction().commit();
		em.close();
	}

	@Override
	protected List getDataByNamedQuery(String sql, Map<String, Object> parms) throws SQLException, NamingException {
		// TODO Auto-generated method stub
		return null;
	}

}
