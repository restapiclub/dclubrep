package com.ipdev.deliveryclub;

import static java.util.Collections.singletonList;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import javax.naming.NamingException;
import javax.persistence.EntityManager;
import javax.ws.rs.Path;

import org.apache.log4j.Logger;

import com.ipdev.domain.Point;

@Path("/point")
public class PointResource extends ResourceBase<Point> {

	private final static Logger logger = Logger.getLogger(PointResource.class);

	private EntityManager em;
	private List<Point> points;

	@Override
	protected List getAllQuery() throws SQLException, NamingException {
		em = getEntityManager();
		em.getTransaction().begin();
		points = em.createNamedQuery("Point.findAll", Point.class).getResultList();
		em.getTransaction().commit();
		em.close();
		return points;
	}

	@Override
	protected List getSingleQuery(int id) throws NamingException {
		em = getEntityManager();
		em.getTransaction().begin();
		points = singletonList(em.find(Point.class, id));
		em.getTransaction().commit();
		em.close();
		return points;
	}

	@Override
	protected void createQuery(Point t) throws SQLException, NamingException {
		em = getEntityManager();
		em.getTransaction().begin();

		Point point = new Point();
		point.setAmount(t.getAmount());
		point.setRestaurant(t.getRestaurant());
		point.setUser(t.getUser());
		em.persist(point);

		em.getTransaction().commit();
		em.close();
	}

	@Override
	protected void deleteQuery(int id) throws SQLException, NamingException {
		em = getEntityManager();
		em.getTransaction().begin();

		Point point = new Point();
		point = em.find(Point.class, id);
		em.remove(point);

		em.getTransaction().commit();
		em.close();
	}

	@Override
	protected void updateQuery(Point t, int id) throws SQLException, NamingException {
		em = getEntityManager();
		em.getTransaction().begin();

		Point point = new Point();
		point = em.find(Point.class, id);
		point.setAmount(t.getAmount() != null ? t.getAmount() : point.getAmount());
		em.persist(point);

		em.getTransaction().commit();
		em.close();
	}

	@Override
	protected List getDataByNamedQuery(String sql, Map<String, Object> parms) throws SQLException, NamingException {
		// TODO Auto-generated method stub
		return null;
	}

}
