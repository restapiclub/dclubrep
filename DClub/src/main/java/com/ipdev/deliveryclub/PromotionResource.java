package com.ipdev.deliveryclub;

import static java.util.Collections.singletonList;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import javax.naming.NamingException;
import javax.persistence.EntityManager;
import javax.ws.rs.Path;

import org.apache.log4j.Logger;

import com.ipdev.domain.Promotion;

@Path("/promotion")
public class PromotionResource extends ResourceBase<Promotion> {

	private final static Logger logger = Logger.getLogger(PromotionResource.class);

	private EntityManager em;
	private List<Promotion> promotions;

	@Override
	protected List getAllQuery() throws SQLException, NamingException {
		em = getEntityManager();
		em.getTransaction().begin();
		promotions = em.createNamedQuery("Promotion.findAll", Promotion.class).getResultList();
		em.getTransaction().commit();
		em.close();
		return promotions;
	}

	@Override
	protected List getSingleQuery(int id) throws NamingException {
		em = getEntityManager();
		em.getTransaction().begin();
		promotions = singletonList(em.find(Promotion.class, id));
		em.getTransaction().commit();
		em.close();
		return promotions;
	}

	@Override
	protected void createQuery(Promotion t) throws SQLException, NamingException {
		em = getEntityManager();
		em.getTransaction().begin();

		Promotion promotion = new Promotion();
		promotion.setName(t.getName());
		promotion.setDateStart(t.getDateStart());
		promotion.setDateEnd(t.getDateEnd());
		promotion.setDecription(t.getDecription());
		promotion.setRestaurant(t.getRestaurant());
		em.persist(promotion);

		em.getTransaction().commit();
		em.close();
	}

	@Override
	protected void deleteQuery(int id) throws SQLException, NamingException {
		em = getEntityManager();
		em.getTransaction().begin();

		Promotion point = new Promotion();
		point = em.find(Promotion.class, id);
		em.remove(point);

		em.getTransaction().commit();
		em.close();
	}

	@Override
	protected void updateQuery(Promotion t, int id) throws SQLException, NamingException {
		em = getEntityManager();
		em.getTransaction().begin();

		Promotion promotion = new Promotion();
		promotion = em.find(Promotion.class, id);
		promotion.setName(t.getName() != null ? t.getName() : promotion.getName());
		promotion.setDateStart(t.getDateStart() != null ? t.getDateStart() : promotion.getDateStart());
		promotion.setDateEnd(t.getDateEnd() != null ? t.getDateEnd() : promotion.getDateEnd());
		promotion.setDecription(t.getDecription() != null ? t.getDecription() : promotion.getDecription());
		em.persist(promotion);

		em.getTransaction().commit();
		em.close();
	}

	@Override
	protected List getDataByNamedQuery(String sql, Map<String, Object> parms) throws SQLException, NamingException {
		// TODO Auto-generated method stub
		return null;
	}

}
