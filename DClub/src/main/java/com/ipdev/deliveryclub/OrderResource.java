package com.ipdev.deliveryclub;

import static java.util.Collections.singletonList;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.naming.NamingException;
import javax.persistence.EntityManager;
import javax.ws.rs.Path;

import org.apache.log4j.Logger;

import com.ipdev.domain.Order;

@Path("/order")
public class OrderResource extends ResourceBase<Order> {

	private final static Logger logger = Logger.getLogger(OrderResource.class);

	private EntityManager em;
	private List<Order> items;

	@Override
	protected List getAllQuery() throws SQLException, NamingException {
		em = getEntityManager();
		em.getTransaction().begin();
		items = em.createNamedQuery("Order.findAll", Order.class).getResultList();
		em.getTransaction().commit();
		em.close();
		return items;
	}

	@Override
	protected List getSingleQuery(int id) throws NamingException {
		em = getEntityManager();
		em.getTransaction().begin();
		items = singletonList(em.find(Order.class, id));
		em.getTransaction().commit();
		em.close();
		return items;
	}

	@Override
	protected void createQuery(Order t) throws SQLException, NamingException {
		em = getEntityManager();
		em.getTransaction().begin();

		Order order = new Order();
		order.setDate(new Date());
		order.setTotal(t.getTotal());
		order.setStatus(t.getStatus());
		order.setRestaurant(t.getRestaurant());
		order.setUser(t.getUser());
		em.persist(order);

		em.getTransaction().commit();
		em.close();
	}

	@Override
	protected void deleteQuery(int id) throws SQLException, NamingException {
		em = getEntityManager();
		em.getTransaction().begin();

		Order order = new Order();
		order = em.find(Order.class, id);
		em.remove(order);

		em.getTransaction().commit();
		em.close();
	}

	@Override
	protected void updateQuery(Order t, int id) throws SQLException, NamingException {
		em = getEntityManager();
		em.getTransaction().begin();

		Order order = new Order();
		order = em.find(Order.class, id);
		order.setStatus(t.getStatus() != null ? t.getStatus() : order.getStatus());
		em.persist(order);

		em.getTransaction().commit();
		em.close();
	}

	@Override
	protected List getDataByNamedQuery(String sql, Map<String, Object> parms) throws SQLException, NamingException {
		// TODO Auto-generated method stub
		return null;
	}

}
