package com.ipdev.deliveryclub;

import static java.util.Collections.singletonList;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import javax.naming.NamingException;
import javax.persistence.EntityManager;
import javax.ws.rs.Path;

import org.apache.log4j.Logger;

import com.ipdev.domain.Menu;

@Path("/menu")
public class MenuResource extends ResourceBase<Menu> {

	private final static Logger logger = Logger.getLogger(MenuResource.class);

	private EntityManager em;
	private List<Menu> menu;

	@Override
	protected List getAllQuery() throws SQLException, NamingException {
		em = getEntityManager();
		em.getTransaction().begin();
		menu = em.createNamedQuery("Menu.findAll", Menu.class).getResultList();
		em.getTransaction().commit();
		em.close();
		return menu;
	}

	@Override
	protected List getSingleQuery(int id) throws NamingException {
		em = getEntityManager();
		em.getTransaction().begin();
		menu = singletonList(em.find(Menu.class, id));
		em.getTransaction().commit();
		em.close();
		return menu;
	}

	@Override
	protected void createQuery(Menu t) throws SQLException, NamingException {
		em = getEntityManager();
		em.getTransaction().begin();

		Menu menu = new Menu();
		menu.setName(t.getName());
		menu.setMenu(t.getMenu());
		menu.setRestaurant(t.getRestaurant());
		em.persist(menu);

		em.getTransaction().commit();
		em.close();
	}

	@Override
	protected void deleteQuery(int id) throws SQLException, NamingException {
		em = getEntityManager();
		em.getTransaction().begin();

		Menu menu = new Menu();
		menu = em.find(Menu.class, id);
		em.remove(menu);

		em.getTransaction().commit();
		em.close();
	}

	@Override
	protected void updateQuery(Menu t, int id) throws SQLException, NamingException {
		em = getEntityManager();
		em.getTransaction().begin();

		Menu menu = new Menu();
		menu = em.find(Menu.class, id);
		menu.setName(t.getName() != null ? t.getName() : menu.getName());
		menu.setMenu(t.getMenu() != null ? t.getMenu() : menu.getMenu());
		em.persist(menu);

		em.getTransaction().commit();
		em.close();
	}

	@Override
	protected List getDataByNamedQuery(String sql, Map<String, Object> parms) throws SQLException, NamingException {
		// TODO Auto-generated method stub
		return null;
	}

}
