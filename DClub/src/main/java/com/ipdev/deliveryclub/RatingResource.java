package com.ipdev.deliveryclub;

import static java.util.Collections.singletonList;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.naming.NamingException;
import javax.persistence.EntityManager;
import javax.ws.rs.Path;

import org.apache.log4j.Logger;

import com.ipdev.domain.Rating;

@Path("/rating")
public class RatingResource extends ResourceBase<Rating> {

	private final static Logger logger = Logger.getLogger(RatingResource.class);

	private EntityManager em;
	private List<Rating> ratings;

	@Override
	protected List getAllQuery() throws SQLException, NamingException {
		em = getEntityManager();
		em.getTransaction().begin();
		ratings = em.createNamedQuery("Rating.findAll", Rating.class).getResultList();
		em.getTransaction().commit();
		em.close();
		return ratings;
	}

	@Override
	protected List getSingleQuery(int id) throws NamingException {
		em = getEntityManager();
		em.getTransaction().begin();
		ratings = singletonList(em.find(Rating.class, id));
		em.getTransaction().commit();
		em.close();
		return ratings;
	}

	@Override
	protected void createQuery(Rating t) throws SQLException, NamingException {
		em = getEntityManager();
		em.getTransaction().begin();

		Rating rating = new Rating();
		rating.setDate(new Date());
		rating.setRate(t.getRate());
		rating.setComment(t.getComment());
		rating.setRestaurant(t.getRestaurant());
		rating.setUser(t.getUser());
		em.persist(rating);

		em.getTransaction().commit();
		em.close();
	}

	@Override
	protected void deleteQuery(int id) throws SQLException, NamingException {
		em = getEntityManager();
		em.getTransaction().begin();

		Rating rating = new Rating();
		rating = em.find(Rating.class, id);
		em.remove(rating);

		em.getTransaction().commit();
		em.close();
	}

	@Override
	protected void updateQuery(Rating t, int id) throws SQLException, NamingException {
		em = getEntityManager();
		em.getTransaction().begin();

		Rating rating = new Rating();
		rating = em.find(Rating.class, id);
		rating.setComment(t.getComment() != null ? t.getComment() : rating.getComment());
		em.persist(rating);

		em.getTransaction().commit();
		em.close();
	}

	@Override
	protected List getDataByNamedQuery(String sql, Map<String, Object> parms) throws SQLException, NamingException {
		// TODO Auto-generated method stub
		return null;
	}

}
