package com.ipdev.domain;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlRootElement;

import com.ipdev.enums.MeasurementType;

/**
 * The persistent class for the items database table.
 * 
 */
@Entity
@Table(name = "items")
@NamedQuery(name = "Item.findAll", query = "SELECT i FROM Item i")
@XmlRootElement
public class Item implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;

	private String description;

	private byte[] img;

	private MeasurementType measurement;

	private String name;

	private BigDecimal price;

	private Integer quantity;

	@Version
	private int version;

	// bi-directional many-to-one association to Discount
	@ManyToOne(fetch = FetchType.LAZY)
	private Discount discount;

	// bi-directional many-to-one association to Menu
	@ManyToOne(fetch = FetchType.LAZY)
	private Menu menu;

	public Item() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public byte[] getImg() {
		return this.img;
	}

	public void setImg(byte[] img) {
		this.img = img;
	}

	public MeasurementType getMeasurement() {
		return this.measurement;
	}

	public void setMeasurement(MeasurementType measurement) {
		this.measurement = measurement;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public BigDecimal getPrice() {
		return this.price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public Integer getQuantity() {
		return this.quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public int getVersion() {
		return this.version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public Discount getDiscount() {
		return this.discount;
	}

	public void setDiscount(Discount discount) {
		this.discount = discount;
	}

	public Menu getMenu() {
		return this.menu;
	}

	public void setMenu(Menu menu) {
		this.menu = menu;
	}

}