package com.ipdev.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlRootElement;

import com.ipdev.enums.Days;

/**
 * The persistent class for the schedules database table.
 * 
 */
@Entity
@Table(name = "schedules")
@NamedQuery(name = "Schedule.findAll", query = "SELECT s FROM Schedule s")
@XmlRootElement
public class Schedule implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;

	@Temporal(TemporalType.TIME)
	@Column(name = "day_end")
	private Date dayEnd;

	@Temporal(TemporalType.TIME)
	@Column(name = "day_start")
	private Date dayStart;

	@Column(name = "day_week")
	private Days dayWeek;

	@Version
	private int version;

	// bi-directional many-to-one association to Restaurant
	@ManyToOne(fetch = FetchType.LAZY)
	private Restaurant restaurant;

	public Schedule() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getDayEnd() {
		return this.dayEnd;
	}

	public void setDayEnd(Date dayEnd) {
		this.dayEnd = dayEnd;
	}

	public Date getDayStart() {
		return this.dayStart;
	}

	public void setDayStart(Date dayStart) {
		this.dayStart = dayStart;
	}

	public Days getDayWeek() {
		return this.dayWeek;
	}

	public void setDayWeek(Days dayWeek) {
		this.dayWeek = dayWeek;
	}

	public int getVersion() {
		return this.version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public Restaurant getRestaurant() {
		return this.restaurant;
	}

	public void setRestaurant(Restaurant restaurant) {
		this.restaurant = restaurant;
	}

}