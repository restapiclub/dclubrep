package com.ipdev.init.conf;

import java.io.IOException;
import java.io.InputStream;

import org.apache.log4j.Logger;
import org.ini4j.Ini;

public class Config {

	private final static Logger logger = Logger.getLogger(Config.class);

	private static Ini ini = new Ini();

	static {
		String pathTC = "/config.ini";
		InputStream resourceAsStream = Config.class.getClassLoader().getResourceAsStream(pathTC);
		org.ini4j.Config conf = new org.ini4j.Config();
		conf.setMultiOption(true);
		ini.setConfig(conf);
		try {
			ini.load(resourceAsStream);
		} catch (IOException e) {
			logger.error(e);
		}
	}

	public static Ini getIni() {
		return ini;
	}

	public static void setIni(Ini ini) {
		Config.ini = ini;
	}
}
