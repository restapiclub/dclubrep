package com.ipdev.reflection;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;

import com.ipdev.deliveryclub.ResourceBase;

public class ParameterizedTypeUtil {

	public static Type getType(Type type) {
		return new ListParameterizedType(type);
	}

	static class ListParameterizedType implements ParameterizedType {

		Type type;

		ListParameterizedType(Type type) {
			this.type = type;
		}

		@Override
		public Type[] getActualTypeArguments() {
			return new Type[] { type };
		}

		@Override
		public Type getRawType() {
			return List.class;
		}

		@Override
		public Type getOwnerType() {
			return ResourceBase.class;
		}
	}
}
