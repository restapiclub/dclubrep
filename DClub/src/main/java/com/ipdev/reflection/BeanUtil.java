package com.ipdev.reflection;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

public class BeanUtil {

	private static Logger log = Logger.getLogger(BeanUtil.class);

	public static String getGetterNameFromField(Field field) {
		String prefix = "get";

		Class fieldType = field.getType();
		String fieldName = field.getName();

		if (fieldType == boolean.class || fieldType == Boolean.class)
			prefix = "is";

		String getterName = prefix + fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1);

		return getterName;
	}

	public static String getGetterNameFromField(String fieldName) {
		String prefix = "get";
		String getterName = prefix + fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1);

		return getterName;
	}

	/**
	 * Ritorna il nome del setter di una proprieta' del bean
	 * 
	 * @param field
	 * @return
	 */
	public static String getSetterNameFromField(String field) {
		String name = "set" + field.substring(0, 1).toUpperCase() + field.substring(1);
		return name;
	}

	public static String getSetterNameFromField(Field field) {
		return getSetterNameFromField(field.getName());
	}

	/**
	 * Ritorna il nome del campo dal nome del metodo setter
	 * 
	 * @param setter
	 * @return
	 */
	public static String getFieldNameFromSetter(String setter) {
		String name = setter.substring(3, 4).toLowerCase() + setter.substring(4);
		return name;
	}

	/**
	 * Invoca un getter
	 * 
	 * @param o
	 * @param m
	 * @return
	 */
	public static Object invokeGetter(Object o, Method m) {
		Object result = null;
		try {
			result = m.invoke(o, (Object[]) null);
		} catch (InvocationTargetException e) {
		} catch (IllegalAccessException ex) {
		}
		return result;
	}

	public static Object invokeStaticMethod(Class beanClass, String methodName, Object... args) {
		Class[] argsTypes = new Class[0];
		if (args != null) {
			argsTypes = new Class[args.length];
			for (int i = 0; i < argsTypes.length; i++) {
				argsTypes[i] = args[i].getClass();
			}
		}
		Method method = BeanUtil.getMethod(beanClass, methodName, argsTypes);
		Object result = null;
		try {
			result = method.invoke(null, args);
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * Invoca un setter
	 * 
	 * @param o
	 * @param m
	 * @param value
	 * @return
	 */
	public static boolean invokeSetter(Object o, Method m, Object value) {
		try {
			m.invoke(o, new Object[] { value });
		} catch (InvocationTargetException ex) {
			return false;
		} catch (IllegalAccessException ex) {
			return false;
		} catch (IllegalArgumentException ex) {
			return false;
		}
		return true;
	}

	/**
	 * Crea un'istanza del bean a partire dal canonicalName
	 * 
	 * @param canonicalName
	 * @return
	 */
	public static Object createInstance(String canonicalName) {
		Object o = null;
		try {
			Class<?> clazz = Class.forName(canonicalName);
			o = createInstance(clazz);
		} catch (ClassNotFoundException e) {
			log.warn(String.format("Impossibile creare istanza della classe [%s]", canonicalName));
		}
		return o;
	}

	/**
	 * Crea un'istanza del bean a partire dalla classe del bean
	 * 
	 * @param bean
	 * @return
	 */
	public static <T> T createInstance(Class<T> beanClass) {
		if (beanClass == null) {
			throw new IllegalArgumentException("Impossibile istanziare oggetto: Class e' null");
		}
		try {
			T bean = beanClass.newInstance();
			return bean;
		} catch (IllegalAccessException e) {
			throw new RuntimeException(String.format("Impossibile creare istanza della classe [%s] ", beanClass), e);
		} catch (InstantiationException e) {
			throw new RuntimeException(String.format("Impossibile creare istanza della classe [%s] ", beanClass), e);
		}
	}

	/**
	 * Ritorna un metodo che accetta 0 parametri
	 * 
	 * @param bean
	 *            Oggetto dal quale prendere il metodo
	 * @param name
	 *            Nome del metodo
	 * @return
	 */
	public static Method getMethod(Class bean, String name) {
		return getMethod(bean, name, (Class[]) null);
	}

	/**
	 * Ritorna un metodo che accetta 0 o piu' parametri
	 * 
	 * @param beanClass
	 *            Oggetto dal quale prendere il metodo
	 * @param name
	 *            Nome del metodo
	 * @param parameterTypes
	 *            Array di tipi accettati dal metodo
	 * @return
	 */
	public static Method getMethod(Class beanClass, String name, Class... parameterTypes) {
		Method method = null;
		try {
			method = beanClass.getMethod(name, parameterTypes);
		} catch (NoSuchMethodException e) {
			throw new RuntimeException(
					String.format("Impossibile leggere il metodo [%s.%s(%s)] ", beanClass, name, parameterTypes), e);
		}
		return method;
	}

	/**
	 * Ritorna i metodi setter di un tipo (tutti i metodi che iniziano con set)
	 * 
	 * @param beanClass
	 * @return
	 */
	public static Method[] getSetters(Class<?> beanClass) {
		Method[] methods = beanClass.getMethods();
		ArrayList<Method> setters = new ArrayList<Method>();
		for (Method method : methods) {
			String methodName = method.getName();
			if (methodName.startsWith("set") && method.getParameterTypes().length == 1) {
				setters.add(method);
			}
		}
		return setters.toArray(new Method[setters.size()]);
	}

	public static Method[] getPublicSetters(Class<?> beanClass) {
		ArrayList<Method> publicSetters = new ArrayList<Method>();
		Method[] setters = getSetters(beanClass);
		for (Method setter : setters) {
			if (Modifier.isPublic(setter.getModifiers())) {
				publicSetters.add(setter);
			}
		}
		return publicSetters.toArray(new Method[publicSetters.size()]);
	}

	public static Field[] getFields(Class<?> beanClass) {
		List<Field> fields = new ArrayList<Field>();

		for (Field field : beanClass.getDeclaredFields()) {
			if (Modifier.isStatic(field.getModifiers()))
				continue;
			List<Class<?>> interfaces = Arrays.asList(field.getType().getInterfaces());
			if (interfaces.contains(Collection.class) || interfaces.contains(Map.class))
				continue;
			fields.add(field);
		}
		return fields.toArray(new Field[0]);
	}

	public static Field getField(Class<?> beanClass, String fieldName) {
		Field field = null;
		while (beanClass.getSuperclass() != null && field == null) {
			try {
				field = beanClass.getDeclaredField(fieldName);
			} catch (NoSuchFieldException e) {
				beanClass = beanClass.getSuperclass();
			}
		}

		return field;
	}

	public static List<Field> getInheritedFields(Class<?> beanClass) {
		List<Field> fields = new ArrayList<Field>();
		while (beanClass.getSuperclass() != null) {
			fields.addAll(Arrays.asList(beanClass.getDeclaredFields()));
			beanClass = beanClass.getSuperclass();
		}

		return fields;
	}

	/**
	 * Controlla se un tipo e' considerato base nel contesto dei bean di dominio
	 * String, boolean, float, double, int, Date, short
	 * 
	 * @param type
	 * @return
	 */
	public static boolean isBaseType(Class type) {
		if (type == String.class || type == char.class || type == Character.class || type == boolean.class
				|| type == Boolean.class || isWrapperNumericType(type) || isPrimitiveNumericType(type)
				|| type == Date.class)
			return true;
		return false;
	}

	/**
	 * Verifies if a Type is a Java Wrapper numeric Type
	 * 
	 * @param type
	 *            The Type to analyze
	 * @return <b>true</b>, if type is a Java Wrapper numeric Type, <b>false</b>
	 *         otherwise
	 */
	public static boolean isWrapperNumericType(Class type) {
		if (type == Float.class || type == Long.class || type == Double.class || type == Integer.class
				|| type == Short.class)
			return true;
		return false;
	}

	/**
	 * Verifies if a Type is a Java primitive numeric Type
	 * 
	 * @param type
	 *            The Type to analyze
	 * @return <b>true</b>, if type is a Java primitive numeric Type, <b>false</b>
	 *         otherwise
	 */
	public static boolean isPrimitiveNumericType(Class type) {
		if (type == float.class || type == long.class || type == double.class || type == int.class
				|| type == short.class)
			return true;
		return false;
	}

	/**
	 * Verifies if a Type is a Java Wrapper or primitive numeric Type
	 * 
	 * @param type
	 *            The Type to analyze
	 * @return <b>true</b>, if type is a Java Wrapper or primitive numeric Type,
	 *         <b>false</b> otherwise
	 */
	public static boolean isNumericType(Class type) {
		return isPrimitiveNumericType(type) || isWrapperNumericType(type);
	}

	public static Object convertToWrapperType(Class type, Object value) {
		if (!isBaseType(type) || value.toString().isEmpty())
			return value;
		if (type == int.class || type == Integer.class)
			if (value instanceof Number) {
				return new Integer(((Number) value).intValue());
			} else {
				return Integer.parseInt(value.toString().trim());
			}
		if (type == long.class || type == Long.class)
			if (value instanceof Number) {
				return new Long(((Number) value).longValue());
			} else {
				return Long.parseLong(value.toString().trim());
			}
		if (type == double.class || type == Double.class)
			if (value instanceof Number) {
				return new Double(((Number) value).doubleValue());
			} else {
				return Double.parseDouble(value.toString().trim());
			}
		if (type == char.class || type == Character.class)
			return value.toString().toCharArray()[0];
		if (type == boolean.class || type == Boolean.class)
			return Boolean.parseBoolean(value.toString().trim());
		if (type == String.class)
			return value.toString().trim();
		return value;
	}

	/**
	 * Recupera un oggetto class a partire dal suo canonicalName (ovvero la classe e
	 * tutto il package)
	 * 
	 * @param canonicalName
	 * @return
	 */
	public static Class loadClass(String canonicalName) {
		try {
			return Class.forName(canonicalName);
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("impossibile caricare classe " + canonicalName, e);
		}
	}

	public static Method getGetterForField(Field field) {
		String getterName = getGetterNameFromField(field);
		Method getter = getMethod(field.getDeclaringClass(), getterName);
		return getter;
	}

	public static Method getGetterForField(Class beanClass, String fieldName) {
		String getterName = getGetterNameFromField(fieldName);
		Method getter = getMethod(beanClass, getterName);
		return getter;
	}

	public static Method getSetterForField(Field field) {
		String setterName = getSetterNameFromField(field.getName());
		Method setter = getMethod(field.getDeclaringClass(), setterName);
		return setter;
	}

	public static Method getSetterForField(Object bean, String fieldName) {
		return getSetterForField(bean.getClass(), fieldName);
	}

	public static Method getSetterForField(Class beanClass, String fieldName) {
		return getSetterForField(getField(beanClass, fieldName));
	}

	public static Class forName(String className) {
		try {
			return Class.forName(className);
		} catch (ClassNotFoundException e) {
			log.debug(e);
			return null;
		}
	}

	public static <T> T createInstance(Class beanClass, Object[] parameters) {
		Class[] parameterTypes = new Class[(parameters != null) ? parameters.length : 0];
		for (int i = 0; i < parameters.length; i++)
			parameterTypes[i] = parameters[i].getClass();
		try {
			Constructor c = beanClass.getConstructor(parameterTypes);
			return (T) c.newInstance(parameters);
		} catch (SecurityException e) {
			throw new RuntimeException(e);
		} catch (NoSuchMethodException e) {
			throw new RuntimeException(e);
		} catch (IllegalArgumentException e) {
			throw new RuntimeException(e);
		} catch (InstantiationException e) {
			throw new RuntimeException(e);
		} catch (IllegalAccessException e) {
			throw new RuntimeException(e);
		} catch (InvocationTargetException e) {
			throw new RuntimeException(e);
		}
	}

	public static Object getFieldValue(Object bean, Field field) {
		Method getter = getGetterForField(field);
		return invokeGetter(bean, getter);
	}

	public static Object getFieldValue(Object bean, String fieldName) {
		try {
			Field field = getField(bean.getClass(), fieldName);
			Method getter = getGetterForField(field);
			return invokeGetter(bean, getter);
		} catch (NullPointerException exception) {
		}

		Method getter = BeanUtil.getGetterForField(bean.getClass(), fieldName);
		return BeanUtil.invokeGetter(bean, getter);
	}

	public static Object getNestedFieldValue(Object bean, String fieldName) {
		Object value = bean;

		String[] stack = Pattern.compile(".", Pattern.LITERAL).split(fieldName);
		for (int i = 0; i < stack.length; ++i) {
			value = getFieldValue(value, stack[i]);
		}

		return value;
	}

	public static Class getFieldType(Object bean, String fieldName) {
		return getFieldType(bean.getClass(), fieldName);
	}

	public static Class getFieldType(Class beanClass, String fieldName) {
		return getField(beanClass, fieldName).getType();
	}

	/**
	 * Ritorna il primo metodo della classe beanClass che abbia nome setterName e
	 * accetti come parametro "type". Funziona quando
	 * paramType.isAssignableFrom(type)
	 * 
	 * @see Class#isAssignableFrom(Class)
	 * @param beanClass
	 * @param setterName
	 * @param type
	 * @return
	 */
	public static Method getSetterAssignableType(Class beanClass, String setterName, Class type) {
		Method[] methods = beanClass.getMethods();
		for (Method method : methods) {
			if (method.getName().equals(setterName) && method.getParameterTypes().length == 1) {
				Class paramType = method.getParameterTypes()[0];
				if (paramType.isAssignableFrom(type)) {
					return method;
				}
			}
		}

		throw new RuntimeException(
				String.format("Impossibile leggere il metodo [%s.%s(%s)] ", beanClass, setterName, type));
	}

	public static boolean objectsAreEqual(Object object, Object object2, String compareFieldName) {
		Object value = BeanUtil.getFieldValue(object, compareFieldName);
		Object value2 = BeanUtil.getFieldValue(object2, compareFieldName);
		if (value.toString().equals(value2.toString()))
			return true;
		return false;
	}

	public static boolean objectsAreEqual(Object object, Object objectToCompare) {
		if (object == null || objectToCompare == null) {
			if (object == objectToCompare)
				return true;
			else
				return false;
		}
		Class objectClass = object.getClass();
		Class objectToCompareClass = objectToCompare.getClass();

		if (!(objectClass.equals(objectToCompareClass)))
			return false;

		if (isBaseType(objectClass))
			if (object.equals(objectToCompare))
				return true;
			else
				return false;

		Field[] fields = objectClass.getDeclaredFields();
		for (Field field : fields) {
			if (Modifier.isStatic(field.getModifiers()))
				continue;
			String fieldName = field.getName();
			if (!objectsAreEqual(getFieldValue(object, fieldName), getFieldValue(objectToCompare, fieldName)))
				return false;
		}
		return true;
	}

	/**
	 * This method compares two instances of the Map interface and returns 1 if the
	 * two Maps are equal.<br>
	 * The method assumes that the key set and the value set of two Maps contain
	 * only Objects that implement the Comparable interface.<br>
	 * The equality of the two maps is considered true if they contain the same
	 * pairs of &ltKey,Value&gt.
	 * 
	 * @param map1
	 *            the first Map
	 * @param map2
	 *            the second Map
	 * @return <b>1</b> if map1 is equal to map2, <b>0</b> otherwise
	 */
	public static int compareMaps(Map<Comparable, Comparable> map1, Map<Comparable, Comparable> map2) {
		if ((map1 == null) || (map2 == null) || (map1.keySet().size() != map2.keySet().size())
				|| (compareCollections(map1.keySet(), map2.keySet()) == 0))
			return 0;
		for (Comparable key : map1.keySet()) {

			Comparable value1 = map1.get(key);

			if (!map2.containsValue(value1))
				return 0;
		}
		return 1;
	}

	/**
	 * This method compares two instances of the Collection interface and returns 1
	 * if the two Collections are equal.<br>
	 * The method assumes that the two Collections contain only Objects that
	 * implement the Comparable interface.<br>
	 * The equality of the two Collections is considered true if they contain the
	 * same data, in any order.
	 * 
	 * @param c1
	 *            the first Collection
	 * @param c2
	 *            the second Collection
	 * @return <b>1</b> if c1 is equal to c2, <b>0</b> otherwise
	 */
	public static int compareCollections(Collection<Comparable> c1, Collection<Comparable> c2) {
		if ((c1 == null) || (c2 == null) || (c1.size() != c2.size()))
			return 0;
		for (Comparable objC1 : c1) {
			if (!c2.contains(objC1))
				return 0;
		}
		return 1;
	}

	public static boolean setFieldValue(Object object, String fieldName, Object value) {
		String setterName = getSetterNameFromField(fieldName);
		Method setter = getSetterAssignableType(object.getClass(), setterName, getFieldType(object, fieldName));
		invokeSetter(object, setter, value);
		return true;
	}

	public static boolean setFieldValue(Object object, Field field, Object value) {
		String setterName = getSetterNameFromField(field.getName());
		Method setter = getSetterAssignableType(object.getClass(), setterName, getFieldType(object, field.getName()));
		invokeSetter(object, setter, value);
		return true;
	}

	/**
	 * path walker
	 * 
	 * @param bean
	 *            - walk is started from this <b>bean</b>
	 * @param path
	 *            - <i>PATH</i> to the desired field, delimited by <i>points</i>.
	 *            Ex: id.anatit.atCodice
	 * @return - target field value
	 */
	public static <T> T getFieldValueFromPath(Object bean, String path) {
		Object currentObject = bean;
		String[] targetFieldPath = Pattern.compile(".", Pattern.LITERAL).split(path);

		for (String fieldName : targetFieldPath) {
			currentObject = BeanUtil.getFieldValue(currentObject, fieldName);
		}
		T targetObjectValue = (T) currentObject;
		return targetObjectValue;
	}

	public static boolean isNull(Object value) {
		Object newValue = convertToWrapperType(value.getClass(), value);
		Class<? extends Object> type = value.getClass();
		if (newValue == null)
			return true;
		if (type == String.class && ((String) newValue).isEmpty())
			return true;
		if (type == Integer.class && ((Integer) newValue).equals(0))
			return true;
		if (type == Long.class)
			return true;
		if (type == Double.class && ((Double) newValue).equals(0.0))
			return true;
		if (type == Character.class && ((Character) newValue) == '\0')
			return true;

		return false;
	}
}