package com.ipdev.reflection;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.ipdev.enums.UserDevice;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface ManagedByUser {
	UserDevice[] TypeManaged() default UserDevice.UNKNOWN;
}
