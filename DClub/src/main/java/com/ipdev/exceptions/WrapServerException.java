package com.ipdev.exceptions;

import com.ipdev.wrapper.ServerResponse;

public class WrapServerException extends ServerResponse {

	public WrapServerException() {
	}

	public WrapServerException(int code, String msg) {
		super();
		this.code = code;
		this.msg = msg;
	}
}
