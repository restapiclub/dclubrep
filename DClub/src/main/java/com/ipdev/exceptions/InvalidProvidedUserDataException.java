package com.ipdev.exceptions;

public class InvalidProvidedUserDataException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InvalidProvidedUserDataException() {

	}

	public InvalidProvidedUserDataException(String message) {
		super(message);
	}
}
