package com.ipdev.wrapper;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ServerResponse {

	public int code;
	public String msg;

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

}
