package com.ipdev.security;

import java.sql.SQLException;

import javax.naming.NamingException;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.PersistenceException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.log4j.Logger;

import com.ipdev.db.DataBaseProvider;
import com.ipdev.exceptions.InvalidProvidedUserDataException;
import com.ipdev.exceptions.WrapServerException;
import com.ipdev.token.security.TokenIssuer;
import com.ipdev.token.security.WrapServerToken;

@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
public abstract class SecurityBase<T> {

	private final static Logger logger = Logger.getLogger(SecurityBase.class);
	protected EntityManager em = DataBaseProvider.getEmf().createEntityManager();
	protected CriteriaBuilder cb = DataBaseProvider.getEmf().getCriteriaBuilder();
	protected EntityTransaction et = em.getTransaction();

	protected abstract void createQuery(T u) throws SQLException, NamingException, InvalidProvidedUserDataException;

	protected abstract void authQuery(T u) throws SQLException, NamingException, InvalidProvidedUserDataException;

	@POST
	@Path("/registration")
	public Response register(T u) {
		try {
			logger.info("Start Call insertSingle query...");
			createQuery(u);
			logger.info("End Call insertSingle query...");
			return Response.status(Status.CREATED).entity(u).build();
		} catch (SQLException | NamingException e) {
			logger.error(e.getMessage());
			return Response.status(Status.NOT_MODIFIED).build();
		} catch (PersistenceException pe) {
			logger.error(pe.getMessage());
			return Response.status(Status.NOT_ACCEPTABLE).build();
		} catch (InvalidProvidedUserDataException ipude) {
			logger.error(ipude.getMessage());
			return Response.status(Status.CONFLICT).entity(ipude.getMessage()).build();
		} catch (Exception ge) {
			logger.error(ge.getMessage());
			return Response.status(Status.FORBIDDEN).build();
		}
	}

	@POST
	@Path("/login")
	public Response auth(T u) {
		try {
			logger.info("Start Call authentification query...");
			authQuery(u);
			String token = new TokenIssuer(u).generate();
			logger.info("End Call authentification query...");
			return Response.status(Status.OK).entity(new WrapServerToken(Status.OK.getStatusCode(), token)).build();
		} catch (SQLException | NamingException e) {
			logger.error(e.getMessage());
			return Response.status(Status.NOT_MODIFIED).build();
		} catch (PersistenceException pe) {
			logger.error(pe.getMessage());
			return Response.status(Status.NOT_ACCEPTABLE).build();
		} catch (InvalidProvidedUserDataException ipude) {
			logger.error(ipude.getMessage());
			return Response.status(Status.UNAUTHORIZED)
					.entity(new WrapServerException(Status.UNAUTHORIZED.getStatusCode(), ipude.getMessage())).build();
		} catch (Exception ge) {
			logger.error(ge.getMessage());
			return Response.status(Status.FORBIDDEN).build();
		}
	}
}
