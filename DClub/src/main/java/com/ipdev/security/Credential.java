package com.ipdev.security;

import javax.xml.bind.annotation.XmlRootElement;

import com.ipdev.enums.UserDevice;

@XmlRootElement
public class Credential {

	private String login;
	private String password;
	private UserDevice device;

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public UserDevice getDevice() {
		return device;
	}

	public void setDevice(UserDevice device) {
		this.device = device;
	}
}
