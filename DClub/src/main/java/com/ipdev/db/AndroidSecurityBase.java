package com.ipdev.db;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import javax.naming.NamingException;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.ws.rs.Path;

import org.apache.log4j.Logger;

import com.ipdev.domain.User;
import com.ipdev.exceptions.InvalidProvidedUserDataException;
import com.ipdev.security.Encript;
import com.ipdev.security.SecurityBase;

@Path("/user")
public class AndroidSecurityBase extends SecurityBase<User> {

	private final static Logger logger = Logger.getLogger(AndroidSecurityBase.class);

	private boolean checkIfUserNotAlreadyExit(User t, boolean isAuth) throws SQLException, NamingException {
		CriteriaQuery criteriaQuery = cb.createQuery();
		Root<User> u = criteriaQuery.from(User.class);
		criteriaQuery.select(u);
		if (isAuth)
			criteriaQuery.where(cb.equal(u.get("login"), cb.parameter(String.class, "login")),
					cb.equal(u.get("password"), cb.parameter(String.class, "password")));
		else
			criteriaQuery.where(cb.equal(u.get("login"), cb.parameter(String.class, "login")));

		Query query = em.createQuery(criteriaQuery);
		query.setParameter("login", t.getLogin());
		if (isAuth)
			query.setParameter("password", Encript.MD5(t.getPassword()));

		List<User> exist = query.getResultList();
		return exist.isEmpty();
	}

	@Override
	public void authQuery(User cr) throws SQLException, NamingException, InvalidProvidedUserDataException {
		if (checkIfUserNotAlreadyExit(cr, true))
			throw new InvalidProvidedUserDataException("Invalid login or password!");
	}

	@Override
	public void createQuery(User u) throws SQLException, NamingException, InvalidProvidedUserDataException {
		if (checkIfUserNotAlreadyExit(u, false)) {
			et.begin();
			User user = new User();
			user.setDateCreated(new Date());
			user.setDateDeleted(u.getDateDeleted());
			user.setLogin(u.getLogin());
			user.setPassword(Encript.MD5(u.getPassword()));
			user.setName(u.getName());
			user.setStatus(u.getStatus());
			em.persist(user);
			et.commit();
			em.close();
		} else {
			throw new InvalidProvidedUserDataException("User " + u.getLogin() + " already exist!");
		}
	}
}
