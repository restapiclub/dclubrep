package com.ipdev.db;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaBuilder;

public class DataBaseProvider {

	private final static String PERSISTENCE_UNIT = "DClub";
	private final static EntityManagerFactory emf = Persistence.createEntityManagerFactory(PERSISTENCE_UNIT);
	private final static CriteriaBuilder criteriaBuilder = emf.getCriteriaBuilder();

	public static EntityManagerFactory getEmf() {
		return emf;
	}

	public static CriteriaBuilder getCriteriabuilder() {
		return criteriaBuilder;
	}

}
