package com.ipdev.enums;

public enum UserStatus {
	UNKNOWN, ANONIM, REGISTERED, VERIFIED, DELETED
}
