package com.ipdev.enums;

public enum MeasurementType {
	GR, KG, TONNE
}
