package com.ipdev.enums;

public enum DiscountType {
	AMOUNT, PERCENTAGE, PACKAGE
}
