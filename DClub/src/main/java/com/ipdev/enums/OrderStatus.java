package com.ipdev.enums;

public enum OrderStatus {
	NEW, PENDING, SUCCESSFUL, UNSUCCESSFUL
}
