package com.ipdev.token.security;

import com.ipdev.wrapper.ServerResponse;

public class WrapServerToken extends ServerResponse {

	public WrapServerToken() {
	}

	public WrapServerToken(int code, String msg) {
		this.code = code;
		this.msg = msg;
	}
}
