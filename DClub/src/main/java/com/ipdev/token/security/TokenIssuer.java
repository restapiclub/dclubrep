package com.ipdev.token.security;

import java.lang.reflect.Field;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;

import com.ipdev.enums.UserDevice;
import com.ipdev.exceptions.InvalidProvidedUserDataException;
import com.ipdev.reflection.BeanUtil;
import com.ipdev.reflection.ManagedByUser;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

public class TokenIssuer {

	private final static Logger logger = Logger.getLogger(TokenIssuer.class);

	private Object o;

	public TokenIssuer(Object o) {
		this.o = o;
	}

	public String generate() throws InvalidProvidedUserDataException, NoSuchFieldException, SecurityException {
		if (this.o == null)
			throw new InvalidProvidedUserDataException("Provided Credential object can't be null!");

		List<UserDevice> types = new ArrayList<>();
		ManagedByUser annotation = o.getClass().getAnnotation(ManagedByUser.class);
		if (annotation != null)
			types = Arrays.asList(annotation.TypeManaged());
		Field field = o.getClass().getDeclaredField("login");
		String login = (String) BeanUtil.invokeGetter(o, BeanUtil.getGetterForField(field));

		if (login == null || types.isEmpty())
			throw new InvalidProvidedUserDataException("Login or type is unknown!");
		Date date = new Date();
		return Jwts.builder().setSubject(login)
				// .setIssuer(issuer)
				.setIssuedAt(date).claim("type", types.stream().map(i -> i.toString()).collect(Collectors.joining(",")))
				.setExpiration(Date.from(LocalDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault())
						.plusHours(15L).atZone(ZoneId.systemDefault()).toInstant()))
				.signWith(SignatureAlgorithm.HS512, KeyGenerator.generateKey()).compact();
	}

}
