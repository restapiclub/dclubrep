package com.ipdev.token.security;

import java.security.Key;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PublicKey;

import org.apache.log4j.Logger;

public class KeyGenerator {

	private final static Logger logger = Logger.getLogger(KeyGenerator.class);

	public static Key generateKey() {
		KeyPairGenerator keyGen = null;
		PublicKey pub = null;
		try {
			keyGen = KeyPairGenerator.getInstance("DSA", "SUN");
			KeyPair pair = keyGen.generateKeyPair();
			pub = pair.getPublic();
		} catch (NoSuchAlgorithmException e) {
			logger.error(e);
		} catch (NoSuchProviderException e) {
			logger.error(e);
		}
		return pub;
	}
}
